#include <iostream>
#include <string>
#include <string.h>

using namespace std;

const char * f() {
    string foo = "toto";
    return foo.c_str();
}
const char * g() {
    string foo = "toto";
    return strdup(foo.c_str());
}

int main () {
    cout << f() << endl;
    cout << g() << endl;
}
