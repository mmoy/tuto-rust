#include <iostream>

using namespace std;

class Dummy {
public:
    // Constructor
    Dummy(string name) {
        this->name = name;
        cout << "Dummy(" << name << ")" << endl;
    }
    // Destructor
    ~Dummy() {
        cout << "~Dummy(" << name << ")" << endl;
    }
private:
    string name;
};
