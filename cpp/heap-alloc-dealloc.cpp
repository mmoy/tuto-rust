#include "dummy.hpp"

int main() {
    Dummy *x = new Dummy("x");
    Dummy *y = new Dummy("y");

    delete x;
    delete y;
}
