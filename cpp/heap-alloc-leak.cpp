#include "dummy.hpp"

int main() {
    Dummy *x = new Dummy("x");
    Dummy *y = new Dummy("y");

    // Oops, forgot deallocation :-( => leak
}
