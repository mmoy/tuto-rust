#include "dummy.hpp"

class DummyRAII {
public:    
    DummyRAII(string name) {
        d = new Dummy(name);
    }
    ~DummyRAII() {
        delete d;
    }

private:
    Dummy *d;
};

int main() {
    DummyRAII x = DummyRAII("x");
    DummyRAII y = DummyRAII("y");

    // Implicit deallocation
}
