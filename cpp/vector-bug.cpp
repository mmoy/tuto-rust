#include <vector>
#include <iostream>

using namespace std;

int main() {
    vector<int> vec = {1, 2, 3, 4};
    for (int val : vec) {
        if (val % 2 == 0) {
            vec.push_back(val);
        }
    }
    for (int val : vec) {
        cout << val << endl;
    }
}
