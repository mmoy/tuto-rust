struct Dummy {
    pub name: String
}
impl Dummy {
    pub fn new(name: &str) -> Dummy {
        println!("Dummy({})", name);
        return Dummy {name: String::from(name)};
    }
}
impl Drop for Dummy {
    fn drop(&mut self) {
        println!("~Dummy({})", self.name);
    }
}

fn allocate_param(v: &mut Box<Dummy>) {
    *v = Box::new(Dummy::new("allocated"));
}
fn allocate_return() -> Box<Dummy> {
    return Box::new(Dummy::new("allocated_return"));
}

fn main() {
    let x = Box::new(Dummy::new("x"));
    let z;
    let y = Box::new(Dummy::new("y"));
    z = Box::new(Dummy::new("z"));
    println!("x={}, y={}, z={}",
        x.name, y.name, z.name);
    drop(z);
    // Nope. println!("z={}", z.name);
}

// Things to try:
// - Use z after dropping it.
// - Play with non-trivial CFG and
//   check what liveness analysis does.
// - Re-assign z after dropping it
//   (you'll need let mut z;)
// - Try to re-allocate z after dropping
//   it using allocate_param. Same with
//   allocate_return.
