struct Dummy { pub name: String }
impl Dummy {
    pub fn new(name: String) -> Dummy {
        println!("Dummy({})", name);
        return Dummy {name: name};
    }
    pub fn get_real_name<'a>(&'a self) 
            -> &'a str {
        &self.name.as_str()
    }
    pub fn get_dummy_name(&self)
            -> &'static str {
        "dummy name"
    }
}
impl Drop for Dummy {
    fn drop(&mut self) {
        println!("~Dummy({})", self.name);
    }
}

fn main () {
    let real_name: &str = "init value";
    let dummy_name: &str;
    {
        let d = Dummy::new(
            String::from("x"));
        // real_name = d.get_real_name();
        dummy_name = d.get_dummy_name();
    }
    println!("real_name={}", real_name);
    println!("dummy_name={}", dummy_name);
}
// Things to try:
// - Remove lifetime annotations from get_real_name()
// - Try exchaning the bodies of get_real_name and get_dummy_name
// - Call d.get_real_name() with scope of d shorter than its result
