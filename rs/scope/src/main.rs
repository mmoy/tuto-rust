struct Dummy {
    pub name: String
}
impl Dummy {
    pub fn new(name: String) -> Dummy {
        println!("Dummy({})", name);
        return Dummy {name: name};
    }
}
impl Drop for Dummy {
    fn drop(&mut self) {
        println!("~Dummy({})", self.name);
    }
}

fn main() {
    let x = Dummy::new(String::from("x"));
    let y = Dummy::new(String::from("y"));
    // drop(x); // No, x still used after
    println!("&x={:p}", &x);
    println!("&y={:p}", &y);
    let z = x; // Move semantics
    println!("&z={:p}", &z);
}
// Things to try:
// - drop() twice the same variable
//   sequentially
// - drop() twice the same variable
//   in different if/else branches
// - Difference between let z = y
//   and let z = &y (display addresses)
// - Move the variable in one branch
//   of if/else, not the other.
// - Is this "scope" thing static
//   or dynamic?
// - In which order do destructions
//   happen?
