fn main() {
    let mut s = String::from("Hello");
    s.push_str(", ");

    let other_ref = &mut s;
    // s.push_str("Nope, not here");
    other_ref.push_str("world");

    s.push_str("!");
    println!("{}", s);
}
// Things to try:
// - Use a variable while being borrowed
// - Play with funny CFG to see if liveness analysis does its job
