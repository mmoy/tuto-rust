fn main() {
    let answer = 42;
    println!("The answer is: {}", answer);
    // answer = 43; // Nope

    let mut mutable_answer = 42;
    println!("The answer is: {}", mutable_answer);
    mutable_answer = 43;
    println!("The new answer is: {}", mutable_answer);

    let long_answer: u64 = 42;
    let short_answer: u32 = 42;
}
