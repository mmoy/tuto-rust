\documentclass[aspectratio=169]{beamer}

\usepackage{moy-slides}
\usepackage{listings-rust}
\usepackage{comment}
\usepackage{ulem}
\usepackage[utf8x]{inputenc}
%\DeclareUnicodeCharacter{00A0}{~}
%\DeclareUnicodeCharacter{00B0}{$^\text{o}$}

\excludecomment{hiddenframe}

\title[Tuto Rust]{Rust \sout{for} \uline{by a} dumm\sout{ies}\uline{y}}

% Brainstorm:
% Program
% Model
% Style
% Coding Guidelines
% Faithfulness
% Power


%\subtitle{} % (optional)

\author{Matthieu Moy}
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute[LIP]{Laboratoire d'Informatique du Parallelisme\\
  Lyon, France}

\date{November 2021}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}

\begin{frame}[fragile]
    \frametitle{Expected Set-Up}

    \begin{itemize}
        \item A clone of my repo:\\
        \verb!git clone https://gitlab.inria.fr/mmoy/tuto-rust.git!
        \item A working install of \texttt{cargo}\\
        {\small\url{https://doc.rust-lang.org/stable/book/ch01-01-installation.html}}
\begin{verbatim}
$ cd tuto-rust/rs/hello_cargo/ && cargo run
    Finished dev [unoptimized + debuginfo] target(s) in 0.00s
     Running `target/debug/hello_cargo`
Hello, world!
\end{verbatim}
        Alternative : \url{https://play.rust-lang.org/}
        \item A working C++ compiler (g++ in my Makefile)\\Alternative: \url{https://replit.com/}
        \item \texttt{cd slides \&\& pdflatex tuto-rust.tex} to get the slides
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Rules for this Talk}

    \begin{itemize}
        \item Interrupt me
        \item Play with examples
        \item Take your time to experiment
    \end{itemize}

\end{frame}

\section{Why a new type system?}

\begin{frame}
    \frametitle{Languages and Performance}

    An attempt to classify languages approaches to performance:

    \begin{itemize}
        \item Don't even try: PHP, Python, Lisp, bash by default (fully interpreted)
        \item Good/decent performance by default: OCaml, Java (static typing)
        \item Optimizeable: C, C++ (runtime overhead almost never imposed to the programmer, full control on low-level stuff)
    \end{itemize}
    \pause
    Rust's positioning: Optimizeable, but with more guarantees and convenience constructs than C \& C++.
\end{frame}

\begin{frame}
    \frametitle{What's wrong with C++?}
    \pause
    Answer 1: nothing, you do get control and safety if you know how to program!
    \begin{itemize}
        \item Control: stack Vs heap allocation, manual \texttt{new} / \texttt{delete}
        \item Safety: RAII (see later)
    \end{itemize}

\end{frame}


\begin{frame}[fragile]
    \frametitle{Stack allocation in C++}
    \framesubtitle{dummy.hpp}

    \lstinputlisting[firstline=5]{../cpp/dummy.hpp}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Stack allocation in C++}
    \framesubtitle{stack-alloc-dealloc.cpp}

    \lstinputlisting{../cpp/stack-alloc-dealloc.cpp}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Heap allocation in C++}
    \framesubtitle{heap-alloc-dealloc.cpp}

    \lstinputlisting{../cpp/heap-alloc-dealloc.cpp}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Heap allocation and memory leak in C++}
    \framesubtitle{heap-alloc-leak.cpp}

    \lstinputlisting{../cpp/heap-alloc-leak.cpp}
\end{frame}

\begin{frame}[fragile]
    \frametitle{RAII: Resource Allocation Is Initialization}
    \framesubtitle{raii.cpp}

    \small
    \begin{columns}[t]
        \column{.5\textwidth}
        \lstinputlisting[lastline=14]{../cpp/raii.cpp}
        \column{.5\textwidth}
        \lstinputlisting[firstline=16]{../cpp/raii.cpp}
    \end{columns}
\end{frame}

\begin{frame}[fragile]
    \frametitle{What's wrong with C++? Nothing}

    I told you, nothing's wrong with C++.
    
    \bigskip

    RAII available as generic ``smart pointers'' (\verb!std::unique_ptr!, \verb!std::shared_ptr!, etc.) and generic containers (\verb!std::vector!).

    \bigskip

    End of presentation. Or not.
\end{frame}

\begin{frame}[fragile]
    \frametitle{Problem 1: Bad Interactions Between Reference to the Same Object}

    \framesubtitle{vector-bug.cpp}

    \lstinputlisting[firstline=6]{../cpp/vector-bug.cpp}
\end{frame}

\begin{frame}
    \frametitle{The Problem is Between the Chair and Keyboard}

    I said ``safety if you know how to program!''. You need to read the documentation:

    \bigskip

    \url{https://en.cppreference.com/w/cpp/container/vector/push_back}\\
    \textit{``If the new size() is greater than capacity(), all iterators and references are invalidated. Otherwise no iterators and references are invalidated.''}

\end{frame}

\begin{frame}[fragile]
    \frametitle{Problem 2: Lifetime of Pointers}

    \framesubtitle{c-str.cpp}

    \lstinputlisting[firstline=7]{../cpp/c-str.cpp}
\end{frame}

\begin{frame}
    \frametitle{Again, this is Documented}

    \url{https://en.cppreference.com/w/cpp/string/basic_string/c_str}\\
    \textit{``The pointer obtained from c\_str() may be invalidated by: [...]''}

    \bigskip

    In general, returning a pointer to Foo in C++ can mean:

    \begin{itemize}
        \item Returning a newly allocated object,
        \item Returning a pointer to an internal buffer that will be overridden by the next call,
        \item Returning a pointer to a static (immutable) object (e.g. \texttt{return "foo";}),
        \item Returning a pointer to a part of an existing data structure ($\leftarrow$ case of \texttt{c\_str()}).
    \end{itemize}

    $\leadsto$ Same type for very different behavior! Can't rely on the type
    system to know how long a pointer is valid and when to deallocate it.
\end{frame}

\begin{frame}[fragile]
    \frametitle{On a side note, about returning pointers}

\begin{verbatim}
NAME
    strdup, strndup, strdupa, strndupa - duplicate a string

SYNOPSIS
    #include <string.h>
    char *strdup(const char *s);
    char *strdupa(const char *s);
DESCRIPTION
    The strdup() function returns a pointer to a new string which
    is a duplicate of the string s.  Memory for the new string is
    obtained with malloc(3), and can be freed with free(3).

    strdupa() and strndupa() are similar, but use alloca(3) to
    allocate the buffer.
\end{verbatim}

\end{frame}

\begin{frame}
    \frametitle{What if ... ?}

    \huge

    What if we forbidded multiple mutable references to the same object, and included a notion of lifetime in the type system?

\end{frame}

\section{Rust}
\lstset{language=Rust}

\begin{frame}
    \frametitle{History and Adoption}

    \begin{itemize}
        \item Born in 2006
        \item Sponsored by Mozilla since 2009
        \item 9.5\% of Firefox's code in June 2021 (\url{https://4e6.github.io/firefox-lang-stats/})
        \item 18$^{th}$ language on GihHub (0.73\%), 26$^{th}$ on TIOBE index (OCaml not even mentioned in either)
    \end{itemize}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Rust Hello World}

    \framesubtitle{rs/hello\_cargo/src/main.rs}

    \lstinputlisting{../rs/hello_cargo/src/main.rs}

    \begin{verbatim}
$ cargo run
   Compiling hello_cargo v0.1.0 (tuto-rust/rs/hello_cargo)
    Finished dev [unoptimized + debuginfo] target(s) in 0.36s
     Running `target/debug/hello_cargo`
Hello, world!
    \end{verbatim}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Variables, type inference}

    \framesubtitle{rs/variables/src/main.rs}

    \lstinputlisting{../rs/variables/src/main.rs}
\end{frame}


\begin{frame}[fragile]
    \frametitle{Ownership}

    \framesubtitle{\small \url{https://doc.rust-lang.org/stable/book/ch04-01-what-is-ownership.html}}

    \begin{itemize}
        \item Each value in Rust has a variable that’s called its owner.
        \item There can only be one owner at a time.
        \item When the owner goes out of scope, the value will be dropped.
    \end{itemize}
\end{frame}


\begin{frame}[fragile]
    \frametitle{C++-like Scopes}

    \framesubtitle{rs/scope/src/main.rs}

    \footnotesize
    \begin{columns}[t]
        \column{.5\textwidth}
        \lstinputlisting[lastline=15]{../rs/scope/src/main.rs}
        \column{.5\textwidth}
        \lstinputlisting[firstline=16]{../rs/scope/src/main.rs}
    \end{columns}
\end{frame}


\begin{frame}[fragile]
    \frametitle{Heap and C++-like RAII}

    \framesubtitle{rs/heap/src/main.rs}

    \footnotesize
    \begin{columns}[t]
        \column{.5\textwidth}
        \lstinputlisting[firstline=23,lastline=32]{../rs/heap/src/main.rs}
        \column{.5\textwidth}
        \lstinputlisting[firstline=34]{../rs/heap/src/main.rs}
    \end{columns}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Borrowing: Punching Holes in Scopes}

    \framesubtitle{rs/string-mut/src/main.rs}

    \lstinputlisting{../rs/string-mut/src/main.rs}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Lifetimes}

    \framesubtitle{rs/lifetime/src/main.rs}

    
    \footnotesize
    \begin{columns}[t]
        \column{.5\textwidth}
        \lstinputlisting[lastline=15]{../rs/lifetime/src/main.rs}
        \column{.5\textwidth}
        \lstinputlisting[firstline=22]{../rs/lifetime/src/main.rs}
    \end{columns}
\end{frame}

\begin{frame}
    \frametitle{Other fun things to look at}

    \begin{itemize}
        \item Enums are actually $\approx$ algebraic data types
        \item No inheritence, but ``traits'' ($\approx$ interfaces in Java)
        \item No exception, but a \texttt{enum Result<T, E> \{
            Ok(T),
            Err(E)
        \}} type.
        \item Functional stuff (lambda, etc.)
        \item An \texttt{unsafe} keyword when the type system bugs you
        \item ``Fearless concurrency''
    \end{itemize}

\end{frame}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% ispell-local-dictionary: "american"
%%% End:
